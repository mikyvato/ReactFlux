1- Install node from `node.org`
   Check version run: 

   ~~~
   node -v
   ~~~
   
2- Init directory
   In empty directory run: 

   ~~~
   npm init
   ~~~
   this action will create the file `package.json` what contain the all packages we need
	
3- Install Gulp
   First intall gulp in global mode
   run: 

   ~~~
   npm install -g gulp
   ~~~

   after that install specific versions 
   run: 

   ~~~
   npm install --save gulp@3.9.0 gulp-connect@2.2.0 gulp-open@1.0.0
   ~~~

   this action add three dependencies in the file `package.json`

4- Create special folder
   In the workspace create a special two folders

   - dist
   - src

5- Configure a gulp buld process
   In the project folder create `gulpfile.json`
	 
6- Create `Index.html` file in folder `src` with initial html tags

7- Add `main.js` file in `src` folder
   remember you need add in the end of file folowing code

   ~~~   
   module.exports = App;
   ~~~

8- Install browserify
   in console run:

   ~~~
   npm install --save browserify@11.0.1 reactify@1.1.1 vinyl-source-stream@1.1.0
   ~~~

   browserify which will bundle our files into one file `bundle.js` - this allows to make a single call
   remember we need add reference in `index.html`
   reactify@1.1.1 will compile or transform our React JSX over to plain JS
   vinyl-source-stream which is going to allow us to use conventional text streams with guulp
   
9- Install Bootstrap and JQuery
   in the console run:
   
   ~~~
   npm install --save bootstrap@3.3.5 jquery@2.1.4 gulp-concat@2.6.0
   ~~~

   we need to add in `main.js` the following code 
   
   ~~~
   $ = jQuery = require('jquery');
   ~~~
   
   this declare jquery globaly because Bootstrap expects it to be there
   the last thing is add the `bundle.css` reference in `index.html`
   
10- ESLint Configuration
   in console run:

   ~~~
   gulp install --save gulp-eslint@0.15.0
   ~~~
   
   lint keep us informed when we make mistakes
   ESLint needs a file to configurated `eslint.cinfig.json`
   
11- Install core libraries
   in the console run:

   ~~~
   npm install --save react@0.13.3 react-router@0.13.3 flux@2.0.3
   ~~~
   
   
https://app.pluralsight.com/library/courses/react-flux-building-applications/table-of-contents
   